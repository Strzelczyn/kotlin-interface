interface MyInterface {
    fun showText()
    fun getValue() {
        println("Write value")
        var valueFromComandLine = readLine()!!.toInt()
        println("You write " + valueFromComandLine)
    }
}

class MyClass : MyInterface {
    override fun showText() {
        println("Hi")
    }
}

fun main(args: Array<String>) {
    var myObject = MyClass()
    myObject.showText()
    myObject.getValue()
}
